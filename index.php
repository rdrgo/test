<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    
</head>
<body>

    <div class="section">
        <div class="container">
            <h2>Creando repositorio en GIT</h2>
            <hr style="background: white;">
            <p>Administrando ramas</p>
            <hr style="background: white;">
            <div class="row">
                <div class="col-md-6">
                    <button class="botones" onclick="mostrarRama()"><strong>¿En qué rama estoy?</strong></button>
                </div>
                <div class="col-md-6">
                    <button class="botones" onclick="mostrarEncargado()"><strong>¿Quién es el encargado?</strong></button>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
    <script>
        function mostrarRama(){
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Estás en la rama master',
                showConfirmButton: false,
                timer: 1500
            })
        }

        function mostrarEncargado(){
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'El encargado de la rama es Rodrigo',
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>
</html>